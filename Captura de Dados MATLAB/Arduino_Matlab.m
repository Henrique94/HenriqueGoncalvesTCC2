close all;
clc;

delete(instrfindall); %limpa lixo na serial
%porta = serial('COM6');   %Indica a porta a ser lida
porta = serial('COM4');   %Indica a porta a ser lida
set(porta, 'InputBufferSize', 100);       %Indica o tamnho do buffer
set(porta, 'FlowControl', 'hardware');   
set(porta, 'BaudRate', 115200);          %Taxa de transmiss�o de dados, neste caso 115200
set(porta, 'Parity', 'none');            %Sem bit de paridade
porta.ReadAsyncMode = 'continuous';

set(porta, 'DataBits', 8);         
set(porta, 'StopBit', 1);
%set(porta, 'Timeout',0.0069);             %Tempo de espera para a leitura de um novo dado (deve ser igual ao do Arduino s� que em segundo)
set(porta, 'Timeout',0.0145);


fopen(porta);                            %abre a porta serial
i=1;                                     %Contador

% while(i <= 100)                         %Aguardar 10s de coleta de dados
%  
%    dado = fgetl(porta);                  %Armazena o dado lido na vari�vel dado        
%    fprintf('%s',dado)                    %Imprime valor lido
%    
%    %x(i)=i*0.100;                         %Obt�m o valor de x (periodo) a cada amostra 
%    %y(i)=str2num(dado);                   %Converte a string (dado) em um n�mero
%                           
%    i=i+1;
%    dado=0;                               %Limpa o valor da vari�vel a
%    
% end

%Cria o arquivo contento os dados lidos
adress='C:\Users\Rafaela Iatceki\Desktop\TCC2_2017\Aquisi�ao de Dados IMU\Matlab\';  %salva o arquivo no endere�o indicado
nome_do_arquivo = strcat(adress,'Dados.txt');
id_do_arquivo = fopen(nome_do_arquivo,'wt'); % abre o arquivo criado no modo de leitura
i=1;
while(i <= 10000)
    dado = fgetl(porta);                  %Armazena o dado lido na vari�vel dado        
    fprintf('%s',dado)
    fprintf(id_do_arquivo,'%s\n',dado); % armazena os dados lidos no arquivo aberto
    %fprintf(id_do_arquivo,'\t%5.3f\t%8.2f\n',x(i),y(i)); % armazena os dados lidos no arquivo aberto
    i=i+1;
    dado=0;
end
fclose(id_do_arquivo); %fecha o arquivo
fprintf('\nArquivo gravado com sucesso.\n\n')
fclose(porta); %fecha a porta serial

%plot(x,y) %plota o gr�fico gerado
%grid on;
