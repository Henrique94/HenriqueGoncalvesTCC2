#include <SoftwareSerial.h>
#include <EEPROM.h>
#include <Wire.h>
#include "Matriz.h"

SoftwareSerial mySerial(10, 11); // RX, TX (Xbee S2C)

#define    MPU9250_ADDRESS           0x68 //Endereço para acessar o acelerômetro e o giroscópio
#define    MAG_ADDRESS               0x0C //Endereço para acessar o magnetômetro
#define    GYRO_FULL_SCALE           0x00 //Valor que configura a escala do giroscópio em +/- 250º/s     
#define    ACC_FULL_SCALE            0x00 //Valor que configura a escala do acelerômetro em +/- 2G
#define    FSR                       A0   //Pino onde é realisado a leitura do FSR
#define    N                         50  // Número de amostras utilizadas no filtro média móvel

//Fator de escala (conversão)
#define FATOR_ACCEL                 2.0/32768.0    //Sensibilidade do acelerômetro
#define FATOR_GYRO                  250.0/32768.0 //Sensibilidade do giroscópio
#define FATOR_MAG                   (10.0*4800.0)/32768.0 // Sensibilidade do magnetômetro (convertido para mG)

typedef float  float_t;  // define todas as variáveis utilizadas no programa para o tipo float

/* Dados da IMU */
float_t acelX, acelY, acelZ;
float_t gyroX, gyroY, gyroZ;
float_t magX, magY, magZ;

/*Dados pós filtro*/
float_t dt, phi, theta, psi, teste;

/*Offset*/
float_t Offset_acelX, Offset_acelY, Offset_acelZ;
float_t Offset_gyroX, Offset_gyroY, Offset_gyroZ;
float_t Offset_magX, Offset_magY, Offset_magZ;
float_t scale_magX, scale_magY, scale_magZ, scale_adj;

/*Triad*/
float_t V1[3], V2[3], r2[3], r3[3];
float_t Mref[3][3], MrefT[3][3], F[3][3];

/*Inicialização das matrizes fixas usadas no filtro Kalman*/
float_t P[4][4] = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};
float_t x[4] = {1, 0, 0, 0};
float_t xp[4] = {0, 0, 0, 0};
float_t z[4] = {0, 0, 0, 0};

uint8_t Buffer[14]; // Buffer para dados do I2C
float_t tempo_final, tempo, tempo_inicial; // Variáveis utilizadas para calcular o tempo de execução do algoritmo
uint32_t timer;

/*Sensor de Força resistivo*/
int filtrado;
int valores[N]; //Valor mais recente: 0 e o valor mais antigo: N-1
float TensaoRef = 5.00, forca;

/*Botão*/
int contador = 0;
int buttonState;
const int interruptPin = 7;  //Interrupção para funcionamento do programa no pino 7
const int interruptPin2 = 0;  //Interrupção para calibração no pino 0 (RX)
const int LedAlerta = 5;       //led conectado ao pino digital 5;
int lastButtonState = LOW;   // O valor prévio da chave no circuito
long lastDebounceTime = 0;   // Variável utilizada na temporização
long debounceDelay = 50;     // tempo para estabilizar e minimizar o efeito bouncing
     
//Função para leitura de dados no barramento I2C
void readI2C(uint8_t Endereco, uint8_t Registrador, uint8_t Nbytes, uint8_t* Dados)
{
  // Seta o endereço do registrador
  Wire.beginTransmission(Endereco);
  Wire.write(Registrador);
  Wire.endTransmission();     
  
  // Realiza a leitura dos Nbytes
  Wire.requestFrom(Endereco, Nbytes); 
  uint8_t i=0;
  while (Wire.available())
    Dados[i++]=Wire.read();
}

//Função para a escita de dados no barramento I2C
void writeI2C(uint8_t Endereco, uint8_t Registrador, uint8_t Dados)
{
  // Set register address
  Wire.beginTransmission(Endereco);
  Wire.write(Registrador);
  Wire.write(Dados);
  Wire.endTransmission();
}

//Função para calibração do acelerômetro e do giroscópio
void calibra_MPU(){  
delay(5000); mySerial.println("Coloque o MPU9250 em uma superficie plana e aguarde...");
delay(5000); 
digitalWrite(LedAlerta, HIGH); delay (100);
digitalWrite(LedAlerta, LOW);
mySerial.println("Iniciando calibracao do acelerometro e giroscopio...");
  
  readI2C(MPU9250_ADDRESS,0x3B,14,Buffer);  //Descarta os primeiros valores lidos
  for(int j = 0; j<=10000; j++){

      readI2C(MPU9250_ADDRESS,0x3B,14,Buffer);
      acelX =   (int16_t)((Buffer[0] << 8) | Buffer[1]);
      acelY =   (int16_t)((Buffer[2] << 8) | Buffer[3]);
      acelZ =   (int16_t)((Buffer[4] << 8) | Buffer[5]); 
      gyroX =   (int16_t)((Buffer[8] << 8) | Buffer[9]);
      gyroY =   (int16_t)((Buffer[10] << 8) | Buffer[11]);
      gyroZ =   (int16_t)((Buffer[12] << 8) | Buffer[13]);
  
      Offset_acelX += acelX;
      Offset_acelY += acelY;
      Offset_acelZ += acelZ;   
      Offset_gyroX += gyroX;
      Offset_gyroY += gyroY;
      Offset_gyroZ += gyroZ;
      }
      
      Offset_acelX = Offset_acelX/10000;
      Offset_acelY = Offset_acelY/10000;
      Offset_acelZ = (Offset_acelZ/10000) - 16384;
      Offset_gyroX = Offset_gyroX/10000;
      Offset_gyroY = Offset_gyroY/10000;
      Offset_gyroZ = Offset_gyroZ/10000;
      
      mySerial.println("Calibracao do acelerometro e giroscopio concluida"); 
      digitalWrite(LedAlerta, HIGH); delay (100);
      digitalWrite(LedAlerta, LOW); delay (100); 
      digitalWrite(LedAlerta, HIGH); delay (100);
      digitalWrite(LedAlerta, LOW); 
}

//Função para a calibração do magnetômetro
void calibra_MAG(){
delay(5000);
uint8_t ST1;
float_t max_magX = -32768.0, max_magY = -32768.0, max_magZ = -32768.0;
float_t min_magX =  32767.0, min_magY =  32767.0, min_magZ =  32767.0;
tempo_final = 20000, tempo = 0; 

delay(5000); mySerial.println("Coloque o MPU9250 com o eixo Z perpendicular a uma superficie plana e gire 360 graus em torno do eixo Z...");
delay(5000);
digitalWrite(LedAlerta, HIGH); delay (100);
digitalWrite(LedAlerta, LOW); delay (100);
mySerial.println("Iniciando a calibracao no eixo X e Y do magnetometro...");
  tempo_inicial = millis();
  while(tempo <= tempo_final){

  uint8_t ST1;
  readI2C(MAG_ADDRESS,0x02,1,&ST1);
  if(ST1&0x01 == true) { // wait for magnetometer data ready bit to be set
    readI2C(MAG_ADDRESS,0x03,7,Buffer);
    uint8_t c = Buffer[6]; // End data read by reading ST2 register
    if(!(c & 0x08)) { // Check if magnetic sensor overflow set, if not then report data
       magX = (Buffer[3]<<8 | Buffer[2]);
       magY = (Buffer[1]<<8 | Buffer[0]);
       magZ = -(Buffer[5]<<8 | Buffer[4]);
    }
  }
   
  //Identificando valor máximo e mínimo para cada eixo do magnetômetro (X e Y)      
      //Eixo X
      if (magX >= max_magX)
        max_magX = magX;
      if (magX <= min_magX)
        min_magX = magX;
         
      //Eixo Y
      if (magY >= max_magY)
        max_magY = magY;
      if (magY <= min_magY)
        min_magY = magY;
          
      tempo = millis() - tempo_inicial;
  } 

  Offset_magX = (max_magX + min_magX) / 2;
  scale_magX = (max_magX - min_magX) / 2;

  Offset_magY = (max_magY + min_magY) / 2;
  scale_magY = (max_magY - min_magY) / 2;

  mySerial.println("Calibracao do eixo X e Y finalizada");
  digitalWrite(LedAlerta, HIGH); delay (100);
  digitalWrite(LedAlerta, LOW); delay (100); 
  digitalWrite(LedAlerta, HIGH); delay (100);
  digitalWrite(LedAlerta, LOW); delay(5000);
  
  mySerial.println("Para calibrar o eixo Z coloque o MPU9250 com eixo Y perpendicular ao plano horizontal e gire 360 graus em torno do eixo Y..."); 
  digitalWrite(LedAlerta, HIGH); delay (100);
  digitalWrite(LedAlerta, LOW); delay (100); 
  mySerial.println("Iniciando a calibracao...");
   
   tempo_inicial = millis();
   tempo = 0;
   while(tempo <= tempo_final){

    uint8_t ST1;
    readI2C(MAG_ADDRESS,0x02,1,&ST1);
    if(ST1&0x01 == true) { // wait for magnetometer data ready bit to be set
    readI2C(MAG_ADDRESS,0x03,7,Buffer);
    uint8_t c = Buffer[6]; // End data read by reading ST2 register
    if(!(c & 0x08)) { // Check if magnetic sensor overflow set, if not then report data
       magX = (Buffer[3]<<8 | Buffer[2]);
       magY = (Buffer[1]<<8 | Buffer[0]);
       magZ = -(Buffer[5]<<8 | Buffer[4]);
    }
    }
    
    //Eixo Z
      if (magZ >= max_magZ)
        max_magZ = magZ;
      if (magZ <= min_magZ)
        min_magZ = magZ;
      
      tempo = millis() - tempo_inicial; 
   }

  Offset_magZ = (max_magZ + min_magZ) / 2;
  scale_magZ = (max_magZ - min_magZ) / 2;
  
  scale_adj = (scale_magX + scale_magY + scale_magZ) / 3;
  scale_magX = scale_adj / scale_magX;
  scale_magY = scale_adj / scale_magY;
  scale_magZ = scale_adj / scale_magZ;     

  mySerial.println("Calibracao do magnetometro concluida");
  digitalWrite(LedAlerta, HIGH); delay (100);
  digitalWrite(LedAlerta, LOW); delay (100); 
  digitalWrite(LedAlerta, HIGH); delay (100);
  digitalWrite(LedAlerta, LOW); 
}

void atualizaFSR(){
  int valor = analogRead(FSR);

 //Filtro Média Móvel

 for(int i = N-1 ; i > 0; i--){
      valores[i] = valores[i-1];      
 }
 valores[0] = valor;

 long soma = 0;
 for(int i = 0; i < N; i++){
      soma = soma + valores[i];
 }
 filtrado = soma / N;     //Valor filtrado (Média Móvel)

 float Tensao =  filtrado * (TensaoRef/ 1023.0);

 float delta = (0.0139 * 0.0139) - (4* 0.00001 * (-0.014 - Tensao)); //Calcula o valor de delta da equação polinomial quadrática
 float x1 = (-0.0139 + sqrt(delta))/(2*0.00001);
 float x2 = (-0.0139 - sqrt(delta))/(2*0.00001);

 //Força corresponderá apenas a parte crescente da curva polinomial
 if(x1>x2){
    forca = x1;
 }else{
    forca = x2;
 }
 
 //forca = ((Tensao - 0.0168) / 0.0027) + 2.6; // (aproximação linear)
}

//Função para a leitura dos sensores acelerômetro e giroscópio
void atualizaMPU6050() {
  readI2C(MPU9250_ADDRESS,0x3B,14,Buffer);
  acelX =   (int16_t)((Buffer[0] << 8) | Buffer[1]);
  acelY =   (int16_t)((Buffer[2] << 8) | Buffer[3]);
  acelZ =   (int16_t)((Buffer[4] << 8) | Buffer[5]);
  gyroX =   (int16_t)((Buffer[8] << 8) | Buffer[9]);
  gyroY =   (int16_t)((Buffer[10] << 8) | Buffer[11]);
  gyroZ =   (int16_t)((Buffer[12] << 8) | Buffer[13]);

  gyroX = (gyroX - Offset_gyroX) * FATOR_GYRO *DEG_TO_RAD; // Convert to deg/s
  gyroY = (gyroY - Offset_gyroY) * FATOR_GYRO *DEG_TO_RAD; // Convert to deg/s
  gyroZ = (gyroZ - Offset_gyroZ) * FATOR_GYRO *DEG_TO_RAD; // Convert to deg/s
                                
  acelX = (acelX - Offset_acelX) * FATOR_ACCEL; // Convert em Força G no eixo X
  acelY = (acelY - Offset_acelY) * FATOR_ACCEL; // Convert em Força G no eixo Y
  acelZ = (acelZ - Offset_acelZ) * FATOR_ACCEL; // Convert em Força G no eixo Z
}

//Função para a leitura do magnetômetro
void atualizaMAG() {
  uint8_t ST1;
  readI2C(MAG_ADDRESS,0x02,1,&ST1);
  if(ST1&0x01 == true) { // wait for magnetometer data ready bit to be set
    readI2C(MAG_ADDRESS,0x03,7,Buffer);
    uint8_t c = Buffer[6]; // End data read by reading ST2 register
    if(!(c & 0x08)) { // Check if magnetic sensor overflow set, if not then report data
       magX = (Buffer[3]<<8 | Buffer[2]);
       magY = (Buffer[1]<<8 | Buffer[0]);
       magZ = -(Buffer[5]<<8 | Buffer[4]);
                   
       magX = (magX - Offset_magX)*scale_magX * FATOR_MAG; //Converte em uT no eixo X
       magY = (magY - Offset_magY)*scale_magY * FATOR_MAG; //Converte em uT no eixo Y
       magZ = (magZ - Offset_magZ)*scale_magZ * FATOR_MAG; //Converte em uT no eixo Z
    }
  }
}

//Implementação do algoritmo TRIAD
void Triad(){
int i, j, k;
float_t s2[3], s3[3], Mobs[3][3];
float_t somaproduto, norma, phi_TRIAD, theta_TRIAD, psi_TRIAD;

          //Produto vetorial entre W1 e W2 (vetor de observação)
          s2[0] = acelY*magZ - acelZ*magY;
          s2[1] = (acelZ*magX - acelX*magZ);
          s2[2] = acelX*magY - acelY*magX;

          norma = sqrt(s2[0]*s2[0] + s2[1]*s2[1] + s2[2]*s2[2]);    //Normalização do vetor s2
          norma = 1.0/norma;
          //Serial.println(norma,4);

          for(i = 0; i<3; i++){
          s2[i] *= norma;                             
          }
          
          //Produto vetorial entre W1 e s2
          s3[0] = acelY*s2[2] - acelZ*s2[1];
          s3[1] = acelZ*s2[0] - acelX*s2[2];
          s3[2] = acelX*s2[1] - acelY*s2[0];

          //Montando a Matriz de Observação
          Mobs[0][0] = acelX;   Mobs[0][1] = s2[0];   Mobs[0][2] = s3[0];
          Mobs[1][0] = acelY;   Mobs[1][1] = s2[1];   Mobs[1][2] = s3[1];
          Mobs[2][0] = acelZ;   Mobs[2][1] = s2[2];   Mobs[2][2] = s3[2];
       
          Matriz.Multiplica((float_t*)Mobs, (float_t*)MrefT, 3, 3, 3, (float_t*)F);
}

//Implementação do Filtro de Kalman
void KalmanFilter(float_t dt){
//Inicialização das matrizes do Filtro de Kalman
float_t H[4][4] = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};
float_t R[4][4] = {{0.1, 0, 0, 0}, {0, 0.1, 0, 0}, {0, 0, 0.1, 0}, {0, 0, 0, 0.1}};
float_t Q[4][4] = {{0.001, 0, 0, 0}, {0, 0.001, 0, 0}, {0, 0, 0.001, 0}, {0, 0, 0, 0.001}};
float_t A[4][4], K[4][4], Pp[4][4];
float_t Matriz_Auxiliar_1[4][4], Matriz_Auxiliar_2[4][4], A_Transposta[4][4], auxiliar_1[4], auxiliar_2[4];
int j;

            //Montando a matriz de tranzição A
             A[0][0] = 1;               A[0][1] = -gyroX*0.5*dt;       A[0][2] = -gyroY*0.5*dt;       A[0][3] = -gyroZ*0.5*dt;
             A[1][0] = gyroX*0.5*dt;    A[1][1] = 1;                   A[1][2] = gyroZ*0.5*dt;        A[1][3] = -gyroY*0.5*dt;
             A[2][0] = gyroY*0.5*dt;    A[2][1] = -gyroZ*0.5*dt;       A[2][2] = 1;                   A[2][3] = gyroX*0.5*dt;
             A[3][0] = gyroZ*0.5*dt;    A[3][1] = gyroY*0.5*dt;        A[3][2] = -gyroX*0.5*dt;       A[3][3] = 1;

            /*Passo 01: Estado de predição - Operação: xp = A*x*/
            for(int i = 0; i<4; i++){
                xp[i] = A[i][0]*x[0] + A[i][1]*x[1] + A[i][2]*x[2] + A[i][3]*x[3];    
            }

            /*Passo 02: Estimação do erro de covariância - Operação: Pp = A*P*A' + Q*/
            Matriz.Multiplica((float_t*)A, (float_t*)P, 4, 4, 4, (float_t*)Matriz_Auxiliar_1);
            Matriz.Transposta((float_t*) A, 4, 4, (float_t*)A_Transposta);
            Matriz.Multiplica((float_t*)Matriz_Auxiliar_1, (float_t*)A_Transposta, 4, 4, 4, (float_t*)Matriz_Auxiliar_2);
            Matriz.Soma((float_t*)Matriz_Auxiliar_2, (float_t*)Q, (float_t*) Pp);

            /*Passo 03: Computar o ganho de Kalman - Operação: K = Pp*H'*inv(H*Pp*H' + R) --> K = Pp*inv(Pp + R) */
            Matriz.Soma((float_t*)Pp, (float_t*)R, (float_t*)Matriz_Auxiliar_1);
            Matriz.Inverte((float_t*)Matriz_Auxiliar_1);
            Matriz.Multiplica((float_t*)Pp, (float_t*)Matriz_Auxiliar_1, 4, 4, 4, (float_t*)K);

            /*Passo 04: Computar a estimativa - Operaçao: x = xp + K*(z - H*xp)  -->  x = xp + K*(z - xp) */            
            for(int j = 0; j<4; j++){
              auxiliar_1[j] = z[j] - xp[j]; 
            }
            
            for(int j = 0; j<4; j++){
              auxiliar_2[j] = K[j][0]*auxiliar_1[0] + K[j][1]*auxiliar_1[1] + K[j][2]*auxiliar_1[2] + K[j][3]*auxiliar_1[3];  
            }
            
            for(int j = 0; j<4; j++){
              x[j] = xp[j] + auxiliar_2[j]; 
            }

            /*Passo 05: Computar o erro de covariância - Operação: P = Pp - K*H*Pp*/            
            Matriz.Multiplica((float_t*)K, (float_t*)Pp, 4, 4, 4, (float_t*)Matriz_Auxiliar_1);
            Matriz.Subtrai((float_t*) Pp, (float_t*) Matriz_Auxiliar_1, (float_t*) P);
}

void setup(){
 mySerial.begin(115200);
 Wire.begin();

 //Configura as interrupções externas
 attachInterrupt(digitalPinToInterrupt(interruptPin),TrataInterrupcao, HIGH);
 attachInterrupt(digitalPinToInterrupt(interruptPin2),TrataInterrupcao2, RISING);
 pinMode(interruptPin,INPUT);
 pinMode(interruptPin2,INPUT);
 pinMode(LedAlerta, OUTPUT);

 //Configuração do Timer 1 para precisão na taxa de amostragem (pág. 131 datasheet atmega 32u4)
 TCCR1A = 0x00;   //Timer operando em modo normal
 TCCR1B = 0x05;    //Prescaler 1:1024;
 TCNT1 = 65311;     // Estouro = Timer1_cont x prescaler x ciclo de máquina => (256 - 31) x 1024 x (1/16000000)0,0144 = 69Hz
 
 //Configura a taxa de amostragem para 1KHz
 writeI2C(MPU9250_ADDRESS,25,7); 
 writeI2C(MPU9250_ADDRESS,26,0x00); 

 //Configura a escala do giroscópio e do acelerometro
 writeI2C(MPU9250_ADDRESS,27,GYRO_FULL_SCALE);
 writeI2C(MPU9250_ADDRESS,28,ACC_FULL_SCALE);

 //Desabilita o sleep mode
 writeI2C(MPU9250_ADDRESS,107,0x01);

 //Configura o magnetometro
 writeI2C(MPU9250_ADDRESS,0x37,0x02); //Desativa o sleep mode e habilita o magnetômetro
 writeI2C(MAG_ADDRESS,0x0A,0x16); //Configura para leitura em modo contínuo e alta resolução(16bits)

}

ISR(TIMER1_OVF_vect)                              //interrupção do TIMER1 
{
  TCNT1 = 65311;                                 // Renicia TIMER
  
//  Serial.print(phi);  Serial.print("\t");
//  Serial.print(theta);Serial.print("\t");
//  Serial.print(psi);Serial.print("\t");
//  Serial.println(forca);

  mySerial.print(phi); mySerial.print("\t");
  mySerial.print(theta); mySerial.print("\t");
  mySerial.print(psi); mySerial.print("\t");
  mySerial.println(forca);
  
}

void TrataInterrupcao2(){  
  contador = 2;
}

void TrataInterrupcao(){  
  contador = 1;
}

void meuPrograma(){
buttonState = 1;

//Carrega offset da EEPROM
          EEPROM.get(0,Offset_gyroX);
          EEPROM.get(8,Offset_gyroY);
          EEPROM.get(16,Offset_gyroZ);
          EEPROM.get(24,Offset_acelX);
          EEPROM.get(32,Offset_acelY);
          EEPROM.get(40,Offset_acelZ);
          EEPROM.get(48,Offset_magX);
          EEPROM.get(56,Offset_magY);
          EEPROM.get(64,Offset_magZ);
          EEPROM.get(72,scale_magX);
          EEPROM.get(80,scale_magY);
          EEPROM.get(88,scale_magZ);
          delay(500);

          int i, j;
          float_t norma;
          
          //Inicializa o vetor de referência da TRIAD
          for(i = 0; i<=100; i++){
            
          atualizaMPU6050();
          atualizaMAG();
                   
          V1[0] += acelX;
          V1[1] += acelY;
          V1[2] += acelZ;
            
          V2[0] += magX;
          V2[1] += magY;
          V2[2] += magZ;            
          }
       
          for(i = 0; i<3; i++){              
          V1[i] /= 100;
          V2[i] /= 100;               
          }
          
          //normalização dos vetores de referencia da TRIAD
          norma = sqrt(V1[0]*V1[0] + V1[1]*V1[1] + V1[2]*V1[2]);       //Normalização do acelerometro
          norma = 1.0/norma;

          //Serial.println(norma,4);

          for(i = 0; i<3; i++){
          V1[i] *= norma;                             
          }
          
          norma = sqrt(V2[0]*V2[0] + V2[1]*V2[1] + V2[2]*V2[2]);       //Normalização do magnetometro
          norma = 1.0/norma;
          //Serial.println(norma,4);

          for(i = 0; i<3; i++){
          V2[i] *= norma;                             
          }

          //Produto vetorial entre V1 e V2
          r2[0] = V1[1]*V2[2] - V1[2]*V2[1];
          r2[1] = V1[2]*V2[0] - V1[0]*V2[2];
          r3[2] = V1[0]*V2[1] - V1[1]*V2[0];

          norma = sqrt(r2[0]*r2[0] + r2[1]*r2[1] + r2[2]*r2[2]);    //Normalização do vetor r2
          norma = 1.0/norma;
          //Serial.println(norma,4);

          for(i = 0; i<3; i++){
          r2[i] *= norma;                             
          }

          //Produto vetorial entre V1 e r2
          r3[0] = V1[1]*r2[2] - V1[2]*r2[1];
          r3[1] = V1[2]*r2[0] - V1[0]*r2[2];
          r3[2] = V1[0]*r2[1] - V1[1]*r2[0];

          //Montando a Matriz de Referência
          Mref[0][0] = V1[0];   Mref[0][1] = r2[0];   Mref[0][2] = r3[0];
          Mref[1][0] = V1[1];   Mref[1][1] = r2[1];   Mref[1][2] = r3[1];
          Mref[2][0] = V1[2];   Mref[2][1] = r2[2];   Mref[2][2] = r3[2];

          Matriz.Transposta((float_t*) Mref, 3, 3, (float_t*)MrefT);

          timer = micros();
          TIMSK1 = 0x01;           // habilita a interrupção do TIMER1
            
     while(buttonState == HIGH){
            tempo_inicial = micros();
            dt = (float_t)(micros() - timer) / 1000000; // Calculate delta time
            timer = micros();
          
            atualizaFSR();
            atualizaMPU6050();
            atualizaMAG();
            
            Triad();
            
            //Converte matriz DCM em quatérnios (adaptado: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/)
            float_t trace = F[0][0] + F[1][1] + F[2][2];
            if( trace > 0 ) {
              float_t s = 0.5 / sqrtf(trace+ 1.0);
              z[0] = 0.25 / s;
              z[1] = (F[1][2] - F[2][1]) * s;
              z[2] = (F[2][0] - F[0][2]) * s;
              z[3] = (F[0][1] - F[1][0]) * s;  
            }else{
              if ( F[0][0] > F[1][1] && F[0][0] > F[2][2] ) {
                  float_t s = 2.0 * sqrt( 1.0 + F[0][0] - F[1][1] - F[2][2]);
                  z[0] = (F[1][2] - F[2][1] ) / s;
                  z[1] = 0.25 * s;
                  z[2] = (F[1][0] + F[0][1] ) / s;
                  z[3] = (F[2][0] + F[0][2] ) / s;
            }else if (F[1][1] > F[2][2]) {
                  float_t s = 2.0 * sqrt( 1.0 + F[1][1] - F[0][0] - F[2][2]);
                  z[0] = (F[2][0] - F[0][2] ) / s;
                  z[1] = (F[1][0] + F[0][1] ) / s;
                  z[2] = 0.25 * s;
                  z[3] = (F[2][1] + F[1][2] ) / s;
            } else {
                  float_t s = 2.0 * sqrt( 1.0 + F[2][2] - F[0][0] - F[1][1] );
                  z[0] = (F[0][1] - F[1][0] ) / s;
                  z[1] = (F[2][0] + F[0][2] ) / s;
                  z[2] = (F[2][1] + F[1][2] ) / s;
                  z[3] = 0.25 * s;
            }
          }
          
            //Aplica o filtro de Kalman
            KalmanFilter(dt);
            
            /*Converte quatérnio em Euler*/
            teste = (x[1]*x[3] - x[0]*x[2]);
            if (teste >= 0.49){
                psi = -2 * atan2(x[1],x[0])* RAD_TO_DEG;
                theta = -PI/2* RAD_TO_DEG;
                phi = 0.0;  
            }
            else if (teste <= -0.49){
              psi = 2 * atan2(x[1],x[0])* RAD_TO_DEG;
              theta = PI/2* RAD_TO_DEG;
              phi = 0.0;  
            }
            else{
            phi   =  atan2( 2*(x[2]*x[3] + x[0]*x[1]), 1 - (2*(x[1] * x[1]) + (x[2] * x[2])) ) * RAD_TO_DEG;
            theta = -asin(2*teste) * RAD_TO_DEG;
            psi =  atan2( 2*(x[1]*x[2] + x[0]*x[3]), 1 - 2*((x[2] * x[2]) + (x[3] * x[3])) ) * RAD_TO_DEG;
            }

//           Serial.print(phi);  Serial.print("\t");
//           Serial.print(theta);Serial.print("\t");
//           Serial.print(psi);Serial.print("\t");
//           Serial.println(forca);

           //Serial.println(dt,4); //periodo de amostragem atual 0.0141s cerca de 70Hz

//           mySerial.print(phi); mySerial.print("\t");
//           mySerial.print(theta); mySerial.print("\t");
//           mySerial.print(psi); mySerial.print("\t");
//           mySerial.println(forca);
//           delay(50);
//                               
          int reading = digitalRead(interruptPin); 

      // Verifica se houve alterações com o valor prévio da chave
        if (reading != lastButtonState) {
          lastDebounceTime = millis();  // Reset na variável de temporização
        }
       
        if ((millis() - lastDebounceTime) > debounceDelay) {
          // Verifica se o estado atual da chave mudou
          if (reading != buttonState) {
            buttonState = reading;  
            }
          }
        // Atualiza a variável com o valor lido na chave
        lastButtonState = reading;
      }

      mySerial.println("Sistema OFF");
}

void loop(){
  if(contador == 1){
    mySerial.println("Sistema ON");
    //digitalWrite(LedAlerta, HIGH);
    meuPrograma();
    TIMSK1 = 0x00;           // desabilita a interrupção do TIMER1
    //digitalWrite(LedAlerta, LOW);
    contador = 0;
  }
  if(contador == 2){
 
            calibra_MPU(); //Calibra acelerômetro e giroscópio
            calibra_MAG(); //Calibra magnetômetro

           //Armazena Offset na EEPROM  
            EEPROM.put(0,Offset_gyroX);
            EEPROM.put(8,Offset_gyroY);
            EEPROM.put(16,Offset_gyroZ);
            EEPROM.put(24,Offset_acelX);
            EEPROM.put(32,Offset_acelY);
            EEPROM.put(40,Offset_acelZ);
            EEPROM.put(48,Offset_magX);
            EEPROM.put(56,Offset_magY);
            EEPROM.put(64,Offset_magZ);
            EEPROM.put(72,scale_magX);
            EEPROM.put(80,scale_magY);
            EEPROM.put(88,scale_magZ);
   contador = 0;
  }
}


