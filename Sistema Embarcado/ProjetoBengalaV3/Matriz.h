#ifndef Matriz_h
#define Matriz_h

typedef float float_t;

class MatrizMath
{
public:
	void Multiplica(float_t* A, float_t* B, int m, int p, int n, float_t* C);
	void Soma(float_t* A, float_t* B, float_t* C);
	void Subtrai(float_t* A, float_t* B, float_t* C);
	void Transposta(float_t* A, int m, int n, float_t* C);
	void Inverte(float_t* A);
};

extern MatrizMath Matriz;
#endif
