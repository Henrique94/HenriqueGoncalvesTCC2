#include "Matriz.h"

typedef float float_t;

MatrizMath Matriz;

void MatrizMath::Multiplica(float_t* A, float_t* B, int m, int p, int n, float_t* C)
{
  int i, j, k;
  for (i = 0; i < m; i++)
    for(j = 0; j < n; j++)
    {
      C[n * i + j] = 0;
      for (k = 0; k < p; k++)
        C[n * i + j] = C[n * i + j] + A[p * i + k] * B[n * k + j];
    }
}

void MatrizMath::Soma(float_t* A, float_t* B, float_t* C)
{
  int i, j;
	for (i = 0; i < 4; i++)
		for(j = 0; j < 4; j++)
			C[4 * i + j] = A[4 * i + j] + B[4 * i + j];
}

void MatrizMath::Subtrai(float_t* A, float_t* B, float_t* C)
{
    int i, j;
	for (i = 0; i < 4; i++)
		for(j = 0; j < 4; j++)
			C[4 * i + j] = A[4 * i + j] - B[4 * i + j];
}

void MatrizMath::Transposta(float_t* A, int m, int n, float_t* C)
{
  int i, j;
  for (i = 0; i < m; i++)
    for(j = 0; j < n; j++)
      C[m * j + i] = A[n * i + j];
}

void MatrizMath::Inverte(float_t* A)
{
    int i, j;
    for (i = 0; i < 4; i++)
		for(j = 0; j < 4; j++)
		{
			if(i == j)
			     A[4 * i + j] = 1.0/A[4 * i + j];
		    else
		          A[4 * i + j] = 0.0;
		}
}
