clc;clear all;close all

load MPU9250;

%Vetor unit�rio de refer�ncia 1
V1 = [0 0 1]';
V1 = V1/norm(V1);

%Vetor unit�rio de refer�ncia 2
V2 = [1.9167 -0.0697  -0.6483]';
V2 = V2/norm(V2);

%Matriz de observa��o
H = eye(4);

%Matriz Q associada ao erro de covari�nicia do ru�do do processo
Q = 0.001 * eye(4);
%Q = 0.0001 * eye(4);
%Matriz R associada ao erro de covari�nica associada � medida
%R = 0.003 * eye(4);
R = 0.1 * eye(4);

%Vetor de estado x
x = [1 0 0 0]';

%Matriz de erro de covari�ncia inicial
P = 1 * eye(4);
