Namostras = 5000;

PhiAnterior = 0;
ThetaAnterior = 0;
PsiAnterior = 0;

dt = 0.0144;
t = 0.0144:dt:Namostras*dt;

%Obtendo a atitude com acelerometro e giroscopio

AtitudeAcel = zeros(Namostras, 2);
AtitudeGiro = zeros(Namostras, 3);
AtitudeMag =  zeros(Namostras, 1);
EulerSaved =  zeros(Namostras, 3);
TriadSaved = zeros(Namostras, 3);


mediaX = 0;
mediaY = 0;
mediaZ = 0;

for i = 1:100
   
    mediaX = mediaX+magX(i);
    mediaY = mediaY+magY(i);
    mediaZ = mediaZ+magZ(i);
    
end

    mediaX = (mediaX/100)/100;
    mediaY = (mediaY/100)/100;
    mediaZ = (mediaZ/100)/100;

for i = 1:Namostras
    
    sinPhi_giro = sin(PhiAnterior); cosPhi_giro = cos(PhiAnterior);
    cosTheta_giro = cos(ThetaAnterior); tanTheta_giro = tan(ThetaAnterior);
    
    acelX = accX(i);
    acelY = accY(i);
    acelZ = accZ(i);
    
    ACC = [acelX acelY acelZ]';
    
    p = gyroX(i)*pi/180;
    q = gyroY(i)*pi/180;
    r = gyroZ(i)*pi/180;
    
    Bpx = magX(i)/100;
    Bpy = magY(i)/100;
    Bpz = magZ(i)/100;
    
    %Calcula atitude com acelerômetro    
    phi_acelerometro  = atan(acelY / sqrt(acelX * acelX + acelZ * acelZ));
    theta_acelerometro = atan2(-acelX, acelZ);
    
    %Angulo de yaw com magnetometro
    Bfy = Bpz * sin(phi_acelerometro) - Bpy * cos(phi_acelerometro);
    Bfx = Bpx * cos(theta_acelerometro) + Bpy * sin(theta_acelerometro) * sin(phi_acelerometro) + Bpz * sin(theta_acelerometro) * cos(phi_acelerometro);
    psi_magnetometro = -atan2(-Bfy,Bfx);
    
    MAG = [Bpx Bpy Bpz]';
    
    F = Triad(V1,V2,ACC,MAG);  %Calcula a matriz de atitude usando o algoritmo TRIAD
    
    phi_TRIAD = atan2(F(2,3),F(3,3)); %roll
    theta_TRIAD = -atan2(F(1,3), sqrt(F(2,3)*F(2,3) + F(3,3)*F(3,3)));
    %theta_TRIAD = -asin(F(1,3));      %pitch
    psi_TRIAD = atan2(F(1,2),F(1,1)); %yaw
    
    tr=trace(F);
    
    z(1)=0.5*sqrt(1+tr);
    z(2)=0.25*(F(2,3)-F(3,2))/z(1);
    z(3)=0.25*(F(3,1)-F(1,3))/z(1);
    z(4)=0.25*(F(1,2)-F(2,1))/z(1);
    
    %z = dcm2quat(F)';          %Converte a matriz de rotação em quatérnios

    %Calcula atitude com giroscópio
    phi_giroscopio   = PhiAnterior   + dt*( p + q*sinPhi_giro*tanTheta_giro + r*cosPhi_giro*tanTheta_giro);
    theta_giroscopio = ThetaAnterior + dt*(     q*cosPhi_giro          - r*sinPhi_giro );
    psi_giroscopio   = PsiAnterior   + dt*(     q*sinPhi_giro/cosTheta_giro + r*cosPhi_giro/cosTheta_giro );
    
    PhiAnterior   = phi_giroscopio;
    ThetaAnterior = theta_giroscopio;
    PsiAnterior   = psi_giroscopio;
    
%Montando a matriz de transição A (Filto de Kalman)
     A = eye(4) + dt * 0.5 * [0 -p -q -r;
                              p  0  r -q;
                              q -r  0  p;
                              r  q -p  0];
      
%Inicia a fusão sensorial usando o filtro de Kalman

        xp = A*x;
        Pp = A*P*A' + Q;

        K = Pp*H'*inv(H*Pp*H' + R);

        x = xp + K*(z' - H*xp);    % x = [ q1 q2 q3 q4 ]
        P = Pp - K*H*Pp;       

        phi   =  atan2( 2*(x(3)*x(4) + x(1)*x(2)), 1 - 2*(x(2)^2 + x(3)^2) );
        theta = -asin(  2*(x(2)*x(4) - x(1)*x(3)) );
        psi   =  atan2( 2*(x(2)*x(3) + x(1)*x(4)), 1 - 2*(x(3)^2 + x(4)^2) );

        EulerSaved (i, :) = [phi theta psi];
        AtitudeAcel(i, :) = [phi_acelerometro theta_acelerometro];
        AtitudeMag (i, :) = (psi_magnetometro);
        AtitudeGiro(i, :) = [phi_giroscopio theta_giroscopio psi_giroscopio];
        TriadSaved (i, :) = [phi_TRIAD theta_TRIAD psi_TRIAD];
        
end


        PhiSaved_giroscopio   = AtitudeGiro(:, 1)* 180/pi;
        ThetaSaved_giroscopio = AtitudeGiro(:, 2)* 180/pi;
        PsiSaved_giroscopio   = AtitudeGiro(:, 3)* 180/pi;
        
        PhiSaved_acelerometro   = AtitudeAcel(:, 1) * 180/pi;
        ThetaSaved_acelerometro = AtitudeAcel(:, 2) * 180/pi;
        
        PsiSaved_magnetometro = AtitudeMag(:, 1) * 180/pi;

        PhiSaved =   EulerSaved(:, 1)* 180/pi;
        ThetaSaved = EulerSaved(:, 2)* 180/pi;
        PsiSaved   = EulerSaved(:, 3)* 180/pi;
        
        PHI_TRIAD = TriadSaved(:, 1)* 180/pi;
        THETA_TRIAD = TriadSaved(:, 2)* 180/pi;
        PSI_TRIAD = TriadSaved(:, 3)* 180/pi;
        
%Plotando os resultados obtidos
  
        figure; plot(t, PhiSaved_giroscopio, 'g')
        hold on; plot(t, PhiSaved, 'b');
        %hold on; plot(t, PHI_TRIAD, 'r');
        hold on; plot(t, PhiSaved_acelerometro, 'r');
        legend('Giroscopio', 'Filtro de Kalman', 'Acelerômetro')
        title('Angulo em torno do eixo X (phi)')
        grid on;
        xlabel('s');
        ylabel('Graus')
        
        figure; plot(t, ThetaSaved_giroscopio, 'g')
        hold on; plot(t, ThetaSaved, 'b');
        %hold on; plot(t, THETA_TRIAD, 'r');
        hold on; plot(t, ThetaSaved_acelerometro, 'r');
        legend('Giroscopio', 'Filtro de Kalman', 'Acelerômetro')
        title('Angulo em torno do eixo Y (theta)')
        grid on;
        xlabel('s');
        ylabel('Graus')

        figure;  plot(t, PsiSaved_giroscopio, 'g')
        hold on; plot(t, PsiSaved, 'b');
        %hold on; plot(t, PSI_TRIAD, 'r');
        hold on; plot(t,PsiSaved_magnetometro, 'r');
        legend('Giroscopio', 'Filtro de Kalman', 'Magnetômetro')
        title('Angulo em torno do eixo Z (psi)')
        grid on;
        xlabel('s');
        ylabel('Graus')