    function [F] = Triad(V1,V2,W1,W2)

r1 = V1;
r2 = cross(V1,V2); % fun��o do produto vetorial
r2 = r2 / norm(r2);
r3 = cross(r1,r2); 

% Montando a Matriz de Refer�ncia
Mref = [r1 r2 r3];

% Tr�ade de observa��o
s1 = W1;
s2 = cross(W1,W2);
s2 = s2 / norm(s2);
s3 = cross(s1,s2);

%Montando a Matriz de Observa��o
Mobs = [s1 s2 s3];

% --- Atitude pelo TRIAD
F = Mobs * Mref';


