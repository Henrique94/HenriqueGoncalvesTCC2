LABCENTER PROTEUS TOOL INFORMATION FILE
=======================================

In case of difficulty, please e-mail support@labcenter.co.uk

Tool set up for Proteus layout 'LayoutV4.pdsprj'.
CADCAM generated at 10:10:38 on segunda-feira, 13 de novembro de 2017.

File List
---------
Top Copper              : LayoutV4 - CADCAM Top Copper.TXT
Bottom Copper           : LayoutV4 - CADCAM Bottom Copper.TXT
Top Silk Screen         : LayoutV4 - CADCAM Top Silk Screen.TXT
Top Solder Resist       : LayoutV4 - CADCAM Top Solder Resist.TXT
Bottom Solder Resist    : LayoutV4 - CADCAM Bottom Solder Resist.TXT
Mechanical 1            : LayoutV4 - CADCAM Mechanical 1.TXT
Drill                   : LayoutV4 - CADCAM Drill.TXT

Photoplotter Setup
------------------
Format: RS274X, ASCII, 2.4, imperial, absolute, eob=*, LZO
Notes:  D=Diameter, S=Side, W=Width, H=Height, C=Chamfer

D10	SQUARE  S=2.03mm                                                          FLASH
D11	SQUARE  S=1.52mm                                                          FLASH
D12	CIRCLE  D=1.78mm                                                          FLASH
D13	CIRCLE  D=2.03mm                                                          FLASH
D14	CIRCLE  D=1.52mm                                                          FLASH
D15	CIRCLE  D=3.81mm                                                          FLASH
D16	CIRCLE  D=0.2mm                                                           DRAW
D17	CIRCLE  D=0.51mm                                                          DRAW
D18	CIRCLE  D=0.25mm                                                          DRAW
D19	CIRCLE  D=0.38mm                                                          DRAW
D70	CIRCLE  D=0.64mm                                                          DRAW
D71	SQUARE  S=2.54mm                                                          FLASH
D20	CIRCLE  D=2.29mm                                                          FLASH
D21	CIRCLE  D=2.54mm                                                          FLASH
D22	CIRCLE  D=4.57mm                                                          FLASH

NC Drill Setup
--------------
Format: ASCII, 2.4, imperial, absolute, eob=<CR><LF>, no zero suppression.
Notes:  Tool sizes are diameters. Layer sets are in brackets - 0=TOP, 15=BOTTOM, 1-14=INNER.

T01	1.02mm (0-15)
T02	0.64mm (0-15)
T03	0.76mm (0-15)
T04	1.52mm (0-15)


[END OF FILE]
