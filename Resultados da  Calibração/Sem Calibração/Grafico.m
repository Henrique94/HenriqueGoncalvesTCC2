%Apresentação dos resultaods parciais

clear all;
close all;
clc;

load EixoZ;
load EixoY;
load EixoX;

plot(MagX1,MagY1,'ro');
grid on;
hold on

plot(MagX2,MagZ2,'go');
hold on

plot(MagY3,MagZ3,'bo');
xlabel('Intensidade do Campo Magnético [mG]');
ylabel('Intensidade do Campo Magnético [mG]');
legend('Eixo XY',' Eixo XZ','Eixo YZ');
