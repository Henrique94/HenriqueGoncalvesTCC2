%Apresentação dos resultaods parciais

clear all;
close all;
clc;

load EixoZ;
load EixoY;
load EixoX;

plot(magX1,magY1,'ro');
axis([-160 160 -160 160])
grid on;
hold on

plot(magX2,magZ2,'go');
hold on
 
plot(magY3,magZ3,'bo');
xlabel('Intensidade do Campo Magnético [mG]');
ylabel('Intensidade do Campo Magnético [mG]');
legend('Eixo XY',' Eixo XZ','Eixo YZ');
